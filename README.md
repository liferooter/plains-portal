<!--
SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# `plains-portal`

This is a simple `org.freedesktop.portal.Settings` implementation that just exports settings from TOML file.

## What is `org.freedesktop.portal.Settings`?

It's one of XDG Desktop Portal APIs that allows to expose some of system settings to applications. Currently
the specification defines settings for system color scheme (dark, light or default), accent color and high-contrast
mode. Custom options are also allowed, but not supported by `plains-portal`. If you want it to support custom
options, feel free to open an issue.

## What `plains-portal` does?

`plains-portal` tracks content of `$XDG_CONFIG_HOME/plains-portal/config.toml`
(or `~/.config/plains-portal/config.toml`) and exports settings from it to
XDG Desktop Portal. When the file is changed, settings from it are reloaded,
so you can just change the file when you want to change your settings and even
integrate it with you desktop environment or anything else as long as it can modify TOML.

Configuration file has following keys:
- `color-scheme`: Which color scheme applications should prefer. Possible values:
  - `no-preference` (default)
  - `dark`
  - `light` (usually means force light color scheme as opposite to ligth-by-default `no-preference`)
- `accent-color`: RGB hex string of default accent color. Defaults to not set.
- `high-contrast`: Boolean value that means whether to use high-constanst color scheme, defaults to `false`.
