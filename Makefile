# SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

prefix ?= /usr/local
bindir ?= $(prefix)/bin
libexecdir ?= $(prefix)/libexec

profile ?= release
target ?= $(shell rustc -vV | sed -n 's/host: //p')
target_dir ?= target

dbus_service_file = data/org.freedesktop.impl.portal.desktop.plains-portal.service
systemd_service_file = data/plains-portal.service

data_files = $(data_files)

binary = '$(target_dir)/$(target)/$(profile)/plains-portal'

$(binary) :
	cargo build --profile $(profile) --target $(target)

%.service : %.service.in
	sed "s:@libexecdir@:$(DESTDIR)$(libexecdir):g" $< > $@

all: $(binary) $(dbus_service_file) $(systemd_service_file)

.PHONY: install
install: all
	    install -D ./LICENSE \
			$(DESTDIR)$(prefix)/share/licenses/plains-portal/LICENSE
	    install -D $(systemd_service_file) \
	    	$(DESTDIR)$(prefix)/lib/systemd/user/plains-portal.service
	    install -D $(dbus_service_file) \
	        $(DESTDIR)$(prefix)/share/dbus-1/services/org.freedesktop.impl.portal.desktop.plains-portal.service
	    install -D ./data/plains-portal.portal \
	        $(DESTDIR)$(prefix)/share/xdg-desktop-portal/portals/plains-portal.portal
		install -D $(binary) \
			$(DESTDIR)$(libexecdir)/plains-portal

.PHONY: clean
clean:
	rm $(dbus_service_file) $(systemd_service_file)
	rm -rf $(target_dir)
