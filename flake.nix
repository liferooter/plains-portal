# SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0
{
  inputs.rust-overlay = {
    url = "github:oxalica/rust-overlay";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  outputs = {
    self,
    nixpkgs,
    rust-overlay,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [rust-overlay.overlays.default];
      };
    in {
      devShells.default = with pkgs;
        mkShell {
          packages = [
            (
              rust-bin.stable.latest.default.override {
                extensions = ["rust-src" "rust-analyzer"];
              }
            )
            gdb
          ];
          RUST_LOG = "debug";
          RUST_BACKTRACE = "full";
        };
      packages = rec {
        plains-portal = with pkgs.rustPlatform;
          buildRustPackage {
            pname = "plains-portal";
            version = "1.0.1";

            src = ./.;

            cargoLock = {
              lockFile = ./Cargo.lock;
            };

            installPhase = ''
              make install prefix=$out
            '';

            meta = with pkgs.lib; {
              description = "Simple settings portal implementation that reads from configuration file";
              homepage = "https://gitlab.com/liferooter/plains-portal";
              license = licenses.gpl3Plus;
              platforms = platforms.linux;
            };
          };
        default = plains-portal;
      };
      formatter = pkgs.alejandra;
    });
}
