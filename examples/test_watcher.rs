use std::{env, error::Error};

use futures::prelude::*;

use plains_portal::Watcher;

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    let args: Vec<String> = env::args().collect();
    let Some(file) = args.get(1) else {
        eprintln!("Usage: {} [FILE]", args[0]);
        return Ok(());
    };

    let mut receiver = Watcher::watch(file);

    pollster::block_on(async move {
        loop {
            receiver.next().await.unwrap();

            println!("File changed");
        }
    })
}
