use std::{
    collections::HashSet,
    ffi::{OsStr, OsString},
    path::{Component, Path, PathBuf},
};

use inotify::{EventMask, WatchDescriptor, WatchMask, Watches};

#[derive(Debug)]
pub struct WatchNode {
    wd: Option<WatchDescriptor>,
    path: PathBuf,
    symlink_target: Option<PathBuf>,
    children: HashSet<OsString>,
}

impl WatchNode {
    const REMOVAL_MASK: WatchMask = WatchMask::DELETE_SELF.union(WatchMask::MOVE_SELF);
    pub const REMOVAL_EVENT_MASK: EventMask = EventMask::DELETE_SELF.union(EventMask::MOVE_SELF);

    const RECHECK_MASK: WatchMask = WatchMask::ATTRIB;
    pub const RECHECK_EVENT_MASK: EventMask = EventMask::ATTRIB;

    const CREATION_MASK: WatchMask = WatchMask::CREATE.union(WatchMask::MOVED_TO);
    pub const CREATION_EVENT_MASK: EventMask = EventMask::CREATE.union(EventMask::MOVED_TO);

    const MODIFY_MASK: WatchMask = WatchMask::MODIFY;
    pub const MODIFY_EVENT_MASK: EventMask = EventMask::MODIFY;

    const DEFAULT_MASK: WatchMask = Self::REMOVAL_MASK.union(Self::RECHECK_MASK);
    const DIR_MASK: WatchMask = Self::DEFAULT_MASK.union(Self::CREATION_MASK);
    const SYMLINK_MASK: WatchMask = Self::DEFAULT_MASK.union(WatchMask::DONT_FOLLOW);
    const FILE_MASK: WatchMask = Self::DEFAULT_MASK
        .union(Self::RECHECK_MASK)
        .union(Self::MODIFY_MASK);

    pub fn new(path: &Path) -> Self {
        Self {
            path: path.to_owned(),
            wd: None,
            symlink_target: None,
            children: HashSet::new(),
        }
    }

    pub fn id(&self) -> Option<i32> {
        self.wd.as_ref().map(|wd| wd.get_watch_descriptor_id())
    }

    pub fn path(&self) -> PathBuf {
        self.path.clone()
    }

    pub fn parent(&self) -> Option<&Path> {
        self.path.parent()
    }

    pub fn children(&self) -> &HashSet<OsString> {
        &self.children
    }

    pub fn symlink_target(&self) -> Option<&Path> {
        self.symlink_target.as_deref()
    }

    pub fn add_child(&mut self, child: &OsStr) {
        self.children.insert(child.to_os_string());
    }

    pub fn remove_child(&mut self, child: &OsStr) {
        self.children.remove(child);
    }

    pub fn reset(&mut self, watches: &mut Watches) {
        if let Some(wd) = self.wd.take() {
            // Removing a non-existing node is OK,
            // because that's what usually happens
            // for deleted or moved files.
            let _ = watches.remove(wd);
        }

        self.symlink_target.take();
    }

    pub fn recheck(&mut self, watches: &mut Watches) {
        self.wd = watches.add(&self.path, self.watch_flags()).ok();

        self.symlink_target = self
            .path
            .is_symlink()
            .then(|| self.path.read_link().ok())
            .flatten()
            .map(|path| {
                let target_path = self
                    .path
                    .parent()
                    .unwrap_or_else(|| "/".as_ref())
                    .join(path);
                let mut target_components = Vec::new();

                for component in target_path.components() {
                    match component {
                        Component::RootDir => target_components.clear(),
                        Component::ParentDir => {
                            target_components.pop();
                        }
                        Component::Normal(component) => {
                            target_components.push(component);
                        }

                        Component::CurDir => unreachable!("Watch path is always absolute"),
                        Component::Prefix(_) => unreachable!("Windows is not supported obviously"),
                    }
                }

                PathBuf::from("/").join(PathBuf::from_iter(target_components))
            });
    }

    fn watch_flags(&self) -> WatchMask {
        if self.path.is_dir() {
            // Track new files
            Self::DIR_MASK
        } else if self.path.is_symlink() {
            // Track symlink itself, not its target
            Self::SYMLINK_MASK
        } else {
            // Track file changes
            Self::FILE_MASK
        }
    }
}
