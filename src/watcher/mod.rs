// SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    collections::{HashMap, HashSet, VecDeque},
    ffi::OsString,
    io,
    path::{Path, PathBuf},
};

use futures::prelude::*;

use anyhow::Result;
use futures::channel::mpsc;
use inotify::{Inotify, Watches};

use self::watch_node::WatchNode;

mod watch_node;

pub struct Watcher {
    sender: mpsc::UnboundedSender<()>,

    watch_target: PathBuf,

    inotify: Inotify,
    watches: Watches,
    buffer: Vec<u8>,

    nodes: HashMap<PathBuf, WatchNode>,
    watch_ids: HashMap<i32, PathBuf>,

    gc_countdown: u32,
}

impl Watcher {
    const GC_THRESHOLD: u32 = 32;
    const INITIAL_BUFFER_SIZE: usize = 32;

    pub fn watch(path: impl AsRef<Path>) -> mpsc::UnboundedReceiver<()> {
        let (sender, receiver) = mpsc::unbounded();
        let watch_target = path.as_ref().to_path_buf();

        assert!(
            watch_target.is_absolute(),
            "Watch target path must be absolute"
        );

        std::thread::spawn(move || {
            let inotify = Inotify::init().unwrap();
            let watches = inotify.watches();

            let mut watcher = Self {
                sender,
                watch_target,
                inotify,
                watches,
                buffer: vec![0; Self::INITIAL_BUFFER_SIZE],
                nodes: HashMap::new(),
                watch_ids: HashMap::new(),
                gc_countdown: Self::GC_THRESHOLD,
            };

            let path = watcher.watch_target.clone();
            watcher.ensure_path(&path);
            watcher.recheck_path("/".as_ref());

            log::debug!("Starting watching path `{}`", path.display());

            loop {
                watcher.handle_events().unwrap();
            }
        });

        receiver
    }

    pub fn handle_events(&mut self) -> Result<()> {
        let events: Vec<inotify::Event<OsString>> =
            match self.inotify.read_events_blocking(&mut self.buffer) {
                Err(err) if err.kind() == io::ErrorKind::InvalidInput => {
                    let new_size = self.buffer.len() * 2;
                    log::debug!("Buffer is too small, so resizing up to {new_size} bytes");
                    self.buffer.resize(new_size, 0);
                    return Ok(());
                }
                res => res?,
            }
            .map(|event| event.to_owned())
            .collect();

        for event in events {
            let Some(path) = self
                .watch_ids
                .get(&event.wd.get_watch_descriptor_id())
                .cloned()
            else {
                log::debug!("Received event for dead watch");
                continue;
            };

            log::debug!("Received event for path `{}`", path.display());

            let mask = event.mask;

            if mask.intersects(WatchNode::REMOVAL_EVENT_MASK) {
                log::debug!("File `{}` was removed", path.display());

                self.reset_path(&path);
                self.notify()?;
            } else if mask.intersects(WatchNode::CREATION_EVENT_MASK) {
                let file_name = event.name.unwrap();
                let new_path = path.join(&file_name);

                log::debug!("File `{}` was created", new_path.display());

                if self.nodes[&path].children().contains(&file_name) {
                    self.recheck_path(&new_path);
                    self.notify()?;
                }
            } else if mask.intersects(WatchNode::RECHECK_EVENT_MASK) {
                log::debug!("Attributes of file `{}` were changed", path.display());

                self.recheck_path(&path);
                self.notify()?;
            } else if mask.intersects(WatchNode::MODIFY_EVENT_MASK) {
                log::debug!("File `{}` was changed", path.display());

                self.notify()?;
            }

            self.gc_countdown -= 1;

            if self.gc_countdown == 0 {
                self.collect_garbadge();
                self.gc_countdown = Self::GC_THRESHOLD;
            }
        }

        Ok(())
    }

    /// Resets nodes of given path and its children.
    ///
    /// Should be called when the file at the path is deleted.
    fn reset_path(&mut self, path: &Path) {
        let mut reset_deque = VecDeque::new();
        reset_deque.push_back(path.to_owned());

        while let Some(path) = reset_deque.pop_front() {
            if let Some(node) = self.nodes.get_mut(&path) {
                log::debug!("Resetting node for `{}`", node.path().display());

                if let Some(wd_id) = node.id() {
                    self.watch_ids.remove(&wd_id);
                }

                node.reset(&mut self.watches);

                for child in node.children() {
                    reset_deque.push_back(node.path().join(child));
                }
            }
        }
    }

    /// Ensures that node for given path exists.
    fn ensure_path(&mut self, path: &Path) {
        let ancestors = path.ancestors().collect::<Vec<_>>().into_iter().rev();

        for path in ancestors {
            if !self.nodes.contains_key(path) {
                log::debug!("Creating node for `{}`", path.display());

                self.nodes.insert(path.to_path_buf(), WatchNode::new(path));

                if let Some(parent) = path.parent() {
                    if let Some(file_name) = path.file_name() {
                        self.nodes.get_mut(parent).unwrap().add_child(file_name);
                    }
                }
            }
        }
    }

    /// Rechecks file information and watch descriptor for given path and its children.
    fn recheck_path(&mut self, path: &Path) {
        self.reset_path(path);

        let mut recheck_deque = VecDeque::new();
        recheck_deque.push_back(path.to_path_buf());

        while let Some(path) = recheck_deque.pop_front() {
            self.ensure_path(&path);

            if let Some(node) = self.nodes.get_mut(&path) {
                log::debug!("Rechecking node for `{}`", path.display());

                node.recheck(&mut self.watches);

                if let Some(wd_id) = node.id() {
                    log::debug!("Node watch descriptor ID: {wd_id}");
                    self.watch_ids.insert(wd_id, node.path());
                }

                if let Some(target) = node.symlink_target() {
                    recheck_deque.push_back(target.to_path_buf());
                }

                for child in node.children() {
                    recheck_deque.push_back(node.path().join(child));
                }
            }
        }
    }

    fn remove_path(&mut self, path: &Path) {
        if let Some(mut node) = self.nodes.remove(path) {
            log::debug!("Removing node for `{}`", path.display());

            if let Some(id) = node.id() {
                self.watch_ids.remove(&id);
            }

            if let Some(parent) = node.parent().and_then(|parent| self.nodes.get_mut(parent)) {
                if let Some(basename) = path.file_name() {
                    parent.remove_child(basename);
                }
            }

            node.reset(&mut self.watches);
        }
    }

    fn collect_garbadge(&mut self) {
        log::debug!("Collecting garbadge");

        let mut connected_paths = HashSet::new();

        let mut inspect_deque = VecDeque::new();
        inspect_deque.push_back(self.watch_target.as_path());

        while let Some(path) = inspect_deque.pop_front() {
            if !connected_paths.contains(path) {
                connected_paths.insert(path.to_path_buf());
            }

            let node = &self.nodes[path];

            if let Some(parent) = node.parent() {
                inspect_deque.push_back(parent);
            }

            if let Some(target) = node.symlink_target() {
                inspect_deque.push_back(target);
            }
        }

        let disconnected_paths: Vec<PathBuf> = self
            .nodes
            .keys()
            .filter(|&p| !connected_paths.contains(p))
            .cloned()
            .collect();

        for path in disconnected_paths {
            self.remove_path(&path);
        }
    }

    fn notify(&mut self) -> Result<()> {
        pollster::block_on(self.sender.send(())).map_err(Into::into)
    }
}
