use anyhow::Result;
use async_std::{fs, path::Path};

use crate::Rgb;

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum ColorScheme {
    #[default]
    NoPreference,
    Dark,
    Light,
}

#[derive(Debug, Clone, Default, serde::Deserialize)]
#[serde(default, deny_unknown_fields, rename_all = "kebab-case")]
pub struct Config {
    pub color_scheme: ColorScheme,
    pub accent_color: Option<Rgb>,
    pub high_contrast: bool,
}

impl Config {
    pub async fn try_load(config_file: impl AsRef<Path>) -> Result<Config> {
        if !config_file.as_ref().exists().await {
            log::info!("Configuration file is not found, so default values will be used");
            return Ok(Config::default());
        }

        let config_contents = fs::read_to_string(config_file).await?;
        let config = toml::from_str(&config_contents)?;

        log::info!("Configuration was successfully loaded");

        Ok(config)
    }
}
