// SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use futures::prelude::*;

use anyhow::{bail, Result};
use async_std::path::Path;

use plains_portal::{dbus, Config, Watcher};

async fn load_config(config_path: impl AsRef<Path>) -> Config {
    Config::try_load(config_path).await.unwrap_or_else(|err| {
        log::error!("Failed to load configuration file: {err}");
        Config::default()
    })
}

#[async_std::main]
async fn main() -> Result<()> {
    env_logger::init();

    let config_path = dirs::config_dir()
        .unwrap()
        .join("plains-portal")
        .join("config.toml");

    log::info!("Configuration file path: {}", config_path.to_string_lossy());

    let portal_state = dbus::SettingsPortal {
        config: load_config(&config_path).await,
    };

    let connection = zbus::ConnectionBuilder::session()?
        .name(dbus::WELL_KNOWN_NAME)?
        .build()
        .await?;

    let portal = connection.object_server();
    portal.at(dbus::OBJECT_PATH, portal_state).await?;

    let mut receiver = Watcher::watch(&config_path);

    loop {
        let Some(()) = receiver.next().await else {
            bail!("Watcher is dropped");
        };

        log::info!("Configuration is changed, so will be reloaded");

        let mut config = load_config(&config_path).await;

        let portal_ref = portal
            .interface::<_, dbus::SettingsPortal>(dbus::OBJECT_PATH)
            .await?;

        let mut portal = portal_ref.get_mut().await;
        let ctx = portal_ref.signal_context();

        std::mem::swap(&mut portal.config, &mut config);
        portal.emmit_diff(ctx, &config).await?;
    }
}
