mod config;
mod rgb;
mod watcher;

pub mod dbus;

pub use config::{ColorScheme, Config};
pub use rgb::Rgb;
pub use watcher::Watcher;
