// SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use nom::{
    bytes::complete::{tag, take_while_m_n},
    combinator::map_res,
    sequence::{preceded, tuple},
    AsChar,
};
use serde::de::{Deserialize, Deserializer, Visitor};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Rgb {
    pub red: u16,
    pub green: u16,
    pub blue: u16,
}

struct RgbVisitor;

impl<'de> Visitor<'de> for RgbVisitor {
    type Value = Rgb;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("RGB color hex string")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        map_res(
            preceded(
                tag::<&str, &str, ()>("#"),
                tuple((
                    take_while_m_n(2, 2, AsChar::is_hex_digit),
                    take_while_m_n(2, 2, AsChar::is_hex_digit),
                    take_while_m_n(2, 2, AsChar::is_hex_digit),
                )),
            ),
            |(red, green, blue): (&str, &str, &str)| {
                Ok::<Rgb, std::num::ParseIntError>(Rgb {
                    red: u16::from_str_radix(red, 16)?,
                    blue: u16::from_str_radix(blue, 16)?,
                    green: u16::from_str_radix(green, 16)?,
                })
            },
        )(v)
        .map_err(|_| E::custom("wrong RGB color hex string format"))
        .map(|(_, color)| color)
    }
}

impl<'de> Deserialize<'de> for Rgb {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(RgbVisitor)
    }
}
