// SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::collections::HashMap;

use zbus::{fdo, zvariant, SignalContext};

use crate::{ColorScheme, Config};

pub mod keys {
    pub const COLOR_SCHEME: &str = "color-scheme";
    pub const ACCENT_COLOR: &str = "accent-color";
    pub const HIGH_CONTRAST: &str = "contrast";
}

pub const APPEARANCE_NAMESPACE: &str = "org.freedesktop.appearance";
pub const WELL_KNOWN_NAME: &str = "org.freedesktop.impl.portal.desktop.plains-portal";
pub const OBJECT_PATH: &str = "/org/freedesktop/portal/desktop";

#[derive(Debug)]
pub struct SettingsPortal {
    pub config: Config,
}

impl SettingsPortal {
    pub fn color_scheme(&self) -> zvariant::OwnedValue {
        match self.config.color_scheme {
            ColorScheme::NoPreference => 0u32,
            ColorScheme::Dark => 1u32,
            ColorScheme::Light => 2u32,
        }
        .into()
    }

    pub fn accent_color(&self) -> zvariant::OwnedValue {
        zvariant::Structure::from(
            self.config
                .accent_color
                .map(|color| {
                    (
                        f64::from(color.red) / 255.0,
                        f64::from(color.green) / 255.0,
                        f64::from(color.blue) / 255.0,
                    )
                })
                .unwrap_or((-1.0, -1.0, -1.0)),
        )
        .try_into()
        .unwrap()
    }

    pub fn high_contrast(&self) -> zvariant::OwnedValue {
        u32::from(self.config.high_contrast).into()
    }

    pub async fn emmit_diff(&self, ctx: &SignalContext<'_>, config: &Config) -> zbus::Result<()> {
        let mut changes = Vec::new();
        if self.config.color_scheme != config.color_scheme {
            changes.push((keys::COLOR_SCHEME, self.color_scheme()));
        }
        if self.config.accent_color != config.accent_color {
            changes.push((keys::ACCENT_COLOR, self.color_scheme()));
        }
        if self.config.high_contrast != config.high_contrast {
            changes.push((keys::HIGH_CONTRAST, self.high_contrast()));
        }

        for (key, value) in changes {
            Self::setting_changed(ctx, APPEARANCE_NAMESPACE, key, value).await?;
        }

        Ok(())
    }
}

#[zbus::interface(name = "org.freedesktop.impl.portal.Settings")]
impl SettingsPortal {
    /// Read method
    async fn read(&self, namespace: &str, key: &str) -> fdo::Result<zvariant::OwnedValue> {
        match (namespace, key) {
            (APPEARANCE_NAMESPACE, keys::COLOR_SCHEME) => Ok(self.color_scheme()),
            (APPEARANCE_NAMESPACE, keys::ACCENT_COLOR) => Ok(self.accent_color()),
            (APPEARANCE_NAMESPACE, keys::HIGH_CONTRAST) => Ok(self.high_contrast()),
            _ => Err(fdo::Error::Failed(String::from("Unknown settings"))),
        }
    }

    /// ReadAll method
    async fn read_all(
        &self,
        namespaces: Vec<String>,
    ) -> fdo::Result<HashMap<String, HashMap<String, zvariant::OwnedValue>>> {
        namespaces
            .into_iter()
            .map(|ns| {
                let correct_namespace = if let Some(prefix) = ns.strip_suffix('*') {
                    APPEARANCE_NAMESPACE.starts_with(prefix)
                } else {
                    ns == APPEARANCE_NAMESPACE
                };

                correct_namespace
                    .then(|| {
                        (
                            String::from(APPEARANCE_NAMESPACE),
                            HashMap::from([
                                (String::from(keys::COLOR_SCHEME), self.color_scheme()),
                                (String::from(keys::ACCENT_COLOR), self.accent_color()),
                                (String::from(keys::HIGH_CONTRAST), self.high_contrast()),
                            ]),
                        )
                    })
                    .ok_or_else(|| fdo::Error::Failed(String::from("Unknown namespace")))
            })
            .collect::<fdo::Result<_>>()
    }

    /// SettingChanged signal
    #[zbus(signal)]
    pub async fn setting_changed(
        ctx: &SignalContext<'_>,
        namespace: &str,
        key: &str,
        value: zvariant::OwnedValue,
    ) -> zbus::Result<()>;

    /// version property
    #[zbus(property, name = "version")]
    fn version(&self) -> u32 {
        2
    }
}
